let minCanvas;

function setup() {
    minCanvas = createCanvas(windowWidth, windowHeight);
    minCanvas.parent("p5Canvas");
}

function preload(){
    img = loadImage("img/bison_1.png");
}

function draw() {
    //clear();
    fill(255,255,255)
    ellipse(mouseX,mouseY,15);
}

console.log("Hello World!")

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}