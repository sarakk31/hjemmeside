from http.server import BaseHTTPRequestHandler, HTTPServer
import json

class RequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        data = json.loads(post_data)

        # Process the data (e.g., pass it to another function or script)
        result = process_data(data['data'])

        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(result.encode())

def process_data(input_data):
    # Add your processing logic here
    return f"You entered: {input_data}"

def run_server():
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, RequestHandler)
    print('Server running at localhost:8000')
    httpd.serve_forever()

if __name__ == '__main__':
    run_server()


<h1>Data Input</h1>
<input type="text" id="inputData">
<button onclick="sendData()">Submit</button>
<div id="output"></div>

<script>
//function sendData() {
  //var inputData = document.getElementById('inputData').value;

  // Send an HTTP POST request to the server with the data
  //fetch('http://localhost:8000/submit_data', {
    //method: 'POST',
    //body: JSON.stringify({ data: inputData }),
//    headers: {
  //    'Content-Type': 'application/json'
//    }
  //})
 // .then(response => response.text())
  //.then(data => {
   // document.getElementById('output').innerHTML = data;
 // });
//}
</script>

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Initialize the browser
driver = webdriver.Chrome()  # You'll need to have ChromeDriver installed

# Open the webpage
driver.get("https://www.arriva.dk/")

# Find the input elements and fill them with your data
departure_input = driver.find_element_by_id("departure")
arrival_input = driver.find_element_by_id("arrival")
date_input = driver.find_element_by_id("date")

departure_input.send_keys("Copenhagen")
arrival_input.send_keys("Aarhus")
date_input.send_keys("2023-09-30")  # Assuming the date format is YYYY-MM-DD

# Submit the form (if applicable)
# For example, if there's a "Search" button, you can do something like:
# search_button = driver.find_element_by_id("search-button")
# search_button.click()

# Now, you can navigate to the ticket pricing page or interact with the page as needed.

# When you're done, you can close the browser
driver.quit()
